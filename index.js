//console.log("Hello World")

//3. Create a variable getCube and use the exponent operator to compute the cube of a number. (A cube is any number raised to 3)
let getCubes = [2];

//4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…

//let cubeNumbers = getCubes.map(num => num ** 3);
//console.log(`The cube of ${getCubes} is ${cubeNumbers}`);


let cubeNumbers = getCubes.map(num => num ** 3);
console.log("The cube of " +getCubes+ " is " +cubeNumbers);

//5. Create a variable address with a value of an array containing details of an address.

let printAddress = {
		address: "258 Washington Ave. NW",
		State: "California", 
		Code: "90011"
	    };

//6. Destructure the array and print out a message with the full address using Template Literals.
console.log(`I live at ${printAddress.address} ${printAddress.State} ${printAddress.Code}`)

//7. Create a variable animal with a value of an object data type with different animal details as it’s properties
 
 const animal = {
 	name: "lolong",
 	type: "saltwater crocodile",
 	details: {
 			weight: "1075 kgs",
 			measurement: "20 ft 3 in"
 			 }
 	};

//8. Destructure the object and print out a message with the details of the animal using Template Literals.
let {name, type, details} = animal
// console.log(name);
// console.log(type);
// console.log(details);
console.log(`${name} was a ${type}. He weight at ${details.weight} with a measuremnt of ${details.measurement}`);

//9. Create an array of numbers.
let numbers = [1, 2, 3, 4, 5];

//10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.

numbers.forEach((number) => {
	console.log(`${number}`)
})

let reducedNumber = numbers.reduce(function(acc, cur){
        return acc + cur;
    })
    console.log(`${reducedNumber}`);

//12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
// const myDog = new Dog();
// console.log(myDog)


// myDog.name = "Frankie",
// myDog.age = "5",
// myDog.breed = "Miniature Dashshund"
// console.log(myDog);

const myDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(myDog);